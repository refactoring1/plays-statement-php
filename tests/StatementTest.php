<?php

namespace Report;

use PHPUnit\Framework\TestCase;

class StatementTest extends TestCase
{
    public function testShouldRender()
    {
        $invoice = array(
            'customer' => 'BigCo',
            'performances' => array(
                array('playID' => "hamlet", 'audience' => 55),
                array('playID' => "as-like", 'audience' => 35),
                array('playID' => "othello", 'audience' => 29),
                array('playID' => "merchant", 'audience' => 19)
            )
        );

        $plays = array(
            'hamlet' => array('name' => "Hamlet", 'type' => "tragedy"),
            'as-like' => array('name' => "As You Like It", 'type' => "comedy"),
            'othello' => array('name' => "Othello", 'type' => "tragedy"),
            'merchant' => array('name' => "The Merchant of Venice", 'type' => "comedy")
        );

        $statement = new Statement();

        $result = $statement->statement($invoice, $plays);
        $this->assertEquals("Statement for BigCo\n" .
            "  Hamlet: $650.00 (55 seats)\n" .
            "  As You Like It: $580.00 (35 seats)\n" .
            "  Othello: $400.00 (29 seats)\n" .
            "  The Merchant of Venice: $357.00 (19 seats)\n" .
            "Amount owed is $1,987.00\n" .
            "You earned 40 credits\n", $result);
    }
}